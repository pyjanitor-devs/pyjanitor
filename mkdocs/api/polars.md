# Polars

::: janitor.polars
    options:
      filters:
      - "!^_"
      members:
        - clean_names
        - complete
        - pivot_longer
        - row_to_names
